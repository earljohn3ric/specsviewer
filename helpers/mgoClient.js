const mongoClient = require('mongodb').MongoClient;
const mgoCon = require('./config').mongo

const mongoConnection = () => {
    return mongoClient.connect(`mongodb://${mgoCon.url}:${mgoCon.port}/${mgoCon.db}`, {
        useNewUrlParser: true
    });
}

module.exports = mongoConnection;