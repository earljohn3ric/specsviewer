module.exports = {
    mongo: {
        url: process.env.MONGO_DB_HOST,
        port: 27017,
        db: 'viewer'
    },
    server: {
        host: process.env.HOST,
        port: process.env.PORT
    }
}