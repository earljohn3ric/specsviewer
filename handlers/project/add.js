const mgo = require('../../helpers/mgoClient')

const addHandler = async (req, rep) => {
    let projectName = req.payload.project_name;
    try {
        const conn = await mgo();
        // find one first
        const count = await conn.db().collection('projects')
            .findOne({
                name: projectName
            });
        if (count == null) {
            const res = await conn.db().collection('projects')
                .insertOne({
                    name: projectName,
                    files: []
                });
                await conn.close();
            return {
                result: {
                    err: false,
                    projectID: res.insertedId,
                    message: 'Successfully Added The Project.'
                }
            }
        } else {
            await conn.close();
            return {
                result: {
                    err: true,
                    message: 'The Project Is Already Exists.'
                }
            }
        }
    } catch (err) {
        return {
            result: {
                error: true,
                message: 'There Is An Internal Problem. Please Try Again Later.'
            }
        }
    }
}

module.exports = addHandler;