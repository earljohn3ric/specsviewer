const mgo = require('../../helpers/mgoClient');
const mongo = require('mongodb');

const getHandler = async (req, rep) => {
    let projects = [];

    try {
        const conn = await mgo();
        const res = await conn.db().collection('projects')
        let cursor = res.find()
            .project({
                _id: 1,
                name: 1
            });
        while (await cursor.hasNext()) {
            await cursor.next().then((item) => {
                projects.push(item);
            })
        }
        await conn.close();
        return {
            result: {
                err: false,
                message: 'ok',
                projects: projects
            }
        }
    } catch (err) {
        console.log(err)
        return {
            result: {
                err: true,
                message: 'An Error Occured.',
                projects: [],
            }
        }
    }
    return projectID;
}

module.exports = getHandler;