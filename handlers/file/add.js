const mgo = require('../../helpers/mgoClient');
const mongo = require('mongodb');
const fs = require('fs');

const addHandler = async (req, rep) => {
    let data = req.payload;
    try {
        let projectID = data.project_id;
        let fileName = data.file.hapi.filename;
        let isNew = parseInt(data.isnew);;
        let modified = new Date().toISOString();
        let realPath = `${__dirname}/../../uploads/${projectID}-${fileName}`;
        let uploadPath = `/uploads/${projectID}-${fileName}`

        let upsert = (isNew) ? false : true;

        let filterQuery;
        let updateQuery;
        if (isNew) {
            filterQuery = {
                _id: new mongo.ObjectID(projectID)
            };
            updateQuery = {
                $addToSet: {
                    files: {
                        name: fileName,
                        modified: modified,
                        path: uploadPath
                    }
                }
            };
        } else {
            filterQuery = {
                _id: new mongo.ObjectID(projectID),
                'files.name': fileName
            };
            updateQuery = {
                $set: {
                    'files.$.modified': modified
                }
            }
        }

        const conn = await mgo();
        const collection = await conn.db().collection('projects');

        await data.file.pipe(
            fs.createWriteStream(realPath)
        );

        let result = await collection.findOneAndUpdate(
            filterQuery,
            updateQuery,
            {
                upsert: upsert,
                new: isNew
            }
        );

        await conn.close();

        return {
            result: {
                error: false,
                message: 'Successfully Added the Specs File.',
                upload_path: uploadPath,
                modified: modified
            }
        }
    } catch (err) {
        console.log(err)
        return {
            result: {
                error: true,
                message: 'There Is An Internal Problem. Please Try Again Later.'
            }
        }
    }
}

module.exports = addHandler;