const mgo = require('../../helpers/mgoClient');
const mongo = require('mongodb');

const verifyHandler = async (req, rep) => {
    let data = req.payload;
    try {
        let projectID = data.project_id;
        let fileName = data.file.hapi.filename;
        const conn = await mgo();
        const collection = await conn.db().collection('projects');
        // check first if exists
        const count = await collection.find({
            _id: new mongo.ObjectID(projectID),
            files: {
                $elemMatch: {
                    name: fileName
                }
            }
        }).count();
        
        await conn.close();
        
        let message = (count > 0)?'An existing file will be modified. Do you want to continue?':'This is a new file. You can now upload.';

        let existing = (count > 0)?true:false;
        
        return {
            result: {
                err: false,
                message: message,
                valid: true,
                existing: existing
            }
        }
    } catch (err) {
        console.log(err)
        return {
            result: {
                err: true,
                message: 'An Error Occured.',
                validation: false
            }
        }
    }
}

module.exports = verifyHandler;