const mgo = require('../../helpers/mgoClient');
const mongo = require('mongodb');

const getHandler = async (req, rep) => {
    let projectID = req.params.projectID;
    let files = [];
    try {
        const conn = await mgo();
        const res = await conn.db().collection('projects')
        let cursor = res.find({
                _id: new mongo.ObjectID(projectID)
            })
            .project({
                _id: 1,
                files: 1
            });
        while (await cursor.hasNext()) {
            await cursor.next().then((result) => {
                result.files.forEach(file => {
                    files.push(file);
                });
            })
        }
        await conn.close();
        return {
            result: {
                err: false,
                message: 'ok',
                files: files
            }
        }
    } catch (err) {
        console.log(err)
        return {
            result: {
                err: true,
                message: 'An Error Occured.',
                files: [],
            }
        }
    }
}

module.exports = getHandler;