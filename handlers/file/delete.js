const mgo = require('../../helpers/mgoClient');
const mongo = require('mongodb');
const fs = require('fs');

const deleteHandler = async (req, rep) => {
    try {
        let projectID = JSON.parse(req.payload).project_id;
        let fileName = JSON.parse(req.payload).file_name;
        let filePath = JSON.parse(req.payload).path;
        let realPath = `${__dirname}/../..${filePath}`;
        const conn = await mgo();

        const res = await conn.db().collection('projects')
            .updateOne({
                _id: new mongo.ObjectID(projectID)
            }, {
                $pull: {
                    files: {
                        name: fileName
                    }
                }
            });

        await conn.close();
        
        fs.unlinkSync(realPath);

        return {
            result: {
                error: false,
                message: 'Successfully Deleted the File.'
            }
        }
    } catch (err) {
        return {
            result: {
                error: true,
                message: 'There Is An Internal Problem. Please Try Again Later.'
            }
        }
    }
}

module.exports = deleteHandler;