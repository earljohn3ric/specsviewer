const Template = require('../helpers/template');
const Path = require('path');

const homeHandler = (request, h) => {
    const relativePath = Path.relative(`${__dirname}`, `${__dirname}/../${Template.folder}/${Template.path}`)

    return h.view('redoc', {
        title: 'OpenAPI Viewer',
        headerTitle: 'OpenAPI Viewer'
    })
};

module.exports = homeHandler;