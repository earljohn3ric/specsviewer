const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');

// templating
const Pug = require('pug');
const Template = require('./helpers/template');

// include custom handlers
const IndexHandler = require('./handlers/home');
const OpenAPIHandler = require('./handlers/openapi-viewer');
const AddProjectHandler = require('./handlers/project/add');
const GetProjectHandler = require('./handlers/project/get');
const GetFileHandler = require('./handlers/file/get');
const AddFileHandler = require('./handlers/file/add');
const VerifyFileHandler = require('./handlers/file/verify');
const DeleteFileHandler = require('./handlers/file/delete');

// config
const serverCon = require('./helpers/config').server;

const server = Hapi.server({
    host: serverCon.host,
    port: serverCon.port
});


let start = async () => {
    // register plugins
    await server.register(Inert);
    await server.register(Vision);

    const options = {
        ops: {
            interval: 1000
        },
        reporters: {
            myConsoleReporter: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{
                    log: '*',
                    response: '*'
                }]
            }, {
                module: 'good-console'
            }, 'stdout']
        }
    };

    await server.register({
        plugin: require('good'),
        options,
    });

    // set templating engine
    server.views({
        engines: {
            pug: Pug
        },
        relativeTo: __dirname,
        path: `${Template.folder}/${Template.path}`
    })

    // - for middleware you can use this:
    // server.ext('onRequest', (req, h) => { /* your code here */ })

    // - general js and css assets
    server.route({
        method: 'GET',
        path: '/assets/{file*}',
        handler: {
            directory: {
                path: './assets/',
                listing: false
            }
        }
    });

    // - home
    server.route({
        method: 'GET',
        path: '/',
        handler: IndexHandler
    });

    // - redoc / openapi viewer
    server.route({
        method: 'GET',
        path: '/redoc/',
        handler: OpenAPIHandler
    })

    // yaml / json specs file
    server.route({
        method: 'GET',
        path: '/uploads/{file*}',
        handler: {
            directory: {
                path: './uploads/',
                listing: false
            }
        }
    })

    // - project related path
    server.route({
        method: 'POST',
        path: '/add-project/',
        handler: AddProjectHandler
    });

    server.route({
        method: 'GET',
        path: '/get-projects/',
        handler: GetProjectHandler
    });

    // - files related path
    server.route({
        method: 'GET',
        path: '/get-files/{projectID}',
        handler: GetFileHandler
    });

    server.route({
        method: 'POST',
        path: '/verify-file/',
        config: {
            payload: {
                output: "stream",
                parse: true,
                allow: "multipart/form-data",
                maxBytes: 100 * 1000 * 1000
            }
        },
        handler: VerifyFileHandler
    });

    server.route({
        method: 'POST',
        path: '/add-file/',
        config: {
            payload: {
                output: "stream",
                parse: true,
                allow: "multipart/form-data",
                maxBytes: 100 * 1000 * 1000
            }
        },
        handler: AddFileHandler
    });

    server.route({
        method: 'DELETE',
        path: '/delete-file/',
        handler: DeleteFileHandler
    });

    try {
        await server.start();
    } catch (err) {
        console.log(err)
        process.exit()
    }

    console.log('Server running at:', server.info.uri);
}

start()