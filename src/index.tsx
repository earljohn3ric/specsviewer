import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ListHeader } from './components/ListHeader';
import { ListBody } from './components/ListBody'
import { ProjectInfo } from './components/ProjectInfo';
import { EventManager } from './components/EventManager';

let eventManager = new EventManager();

ReactDOM.render(
    <ListHeader eventManger={eventManager}/>,
    document.querySelector('div#ProjectHeader')
);

ReactDOM.render(
    <ListBody eventManger={eventManager}/>,
    document.querySelector('div#ProjectBody')
);

ReactDOM.render(
    <ProjectInfo eventManager={eventManager}/>,
    document.querySelector('div#ProjectInfo')
)