import * as React from 'react';
import fetch from 'node-fetch';

import { ListBodyState, GetProjectResult, ListBodyProps } from '../interfaces/interfaces';

export class ListBody extends React.Component<ListBodyProps, ListBodyState> {
    constructor (props: any) {
        super(props);

        this.state = {
            hasContent: false,
            contents: []
        }
    }

    componentDidMount() {
        this.props.eventManger.getEmitter().addListener('new-project', ((e: any) => this.onProjectAdd(e)))
        fetch('/get-projects/')
        .then((response: any) => {
            response.json().then((data: GetProjectResult) => {
                let result = data.result;
                if (!result.err && result.projects.length > 0) {
                    this.setState({
                        hasContent: true,
                        contents: result.projects
                    });
                }
            })
        })
        .catch((err: any) => {
            console.log(err)
        });
    }

    onProjectAdd(e: any) {
        console.log(e);
        let projects = this.state.contents;
        projects.push({
            _id: e.projectID,
            name: e.projectName
        });
        this.setState({
            contents: projects,
            hasContent: true
        });
    }

    clickProjectHandler(e: any, id: string, name: string) {
        e.preventDefault();
        this.props.eventManger.getEmitter().emit('view-project', {
            id: id,
            name: name
        })
    }

    render() {
        let projectRows: Array<any> = [];
        this.state.contents.forEach((project) => {
            projectRows.push(
                <li key={project._id}>
                    <a className='projectName' href="#" onClick={((e)=>this.clickProjectHandler(e, project._id, project.name))}>{project.name}</a>
                </li>
            )
        });
        return (
            <aside className="menu">
                <p className="menu-label">
                    List of Projects
                </p>
                <span>{((this.state.hasContent && this.state.contents.length > 0)?'':'No Projects To Display')}</span>
                <ul className='menu-list'>
                    {projectRows}
                </ul>
            </aside>
        )
    }
}