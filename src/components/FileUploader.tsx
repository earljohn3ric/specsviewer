import * as React from 'react';

import { FileUploaderProps, FileUploadState, VerifyFileResult, AddFileResult } from '../interfaces/interfaces';

export class FileUploader extends React.Component<FileUploaderProps, FileUploadState> {
    constructor(props: any) {
        super(props);

        this.state = {
            filename: 'No File Selected',
            size: 0,
            valid: null,
            message: '',
            projectID: '',
            toUpload: null,
            alreadyExisting: null
        }
    }

    componentDidMount() {
        this.props.eventManager.getEmitter().addListener('view-project', ((e: any) => this.onProjectView(e)));
    }

    onProjectView(e: any) {
        this.setState({
            projectID: e.id
        });
    }

    async onFileSelect(e: any) {
        // single file
        let file = await e.target.files[0];
        this.setState({
            filename: file.name,
            size: file.size,
            toUpload: file,
            message: 'Checking file...'
        });

        let formData: FormData = new FormData();
        formData.append('file', this.state.toUpload);
        formData.append('project_id', this.state.projectID);
        fetch('/verify-file/', {
            method: 'POST',
            body: formData
        })
        .then((response: any) => {
            response.json().then((data: VerifyFileResult) => {
                let result = data.result;
                if (!result.err) {
                    this.setState({
                        valid: result.valid,
                        message: result.message,
                        alreadyExisting: result.existing
                    });
                }
            })
        })
        .catch((err: any) => {
            console.log(err);
        });
    }

    onCancelUpload(e: any) {
        e.preventDefault();
        this.setState({
            filename: 'No File Selected',
            size: 0,
            valid: false,
            alreadyExisting: null,
            message: ''
        });

        let FileUploader: HTMLInputElement = document.querySelector('input[name="specs_file"]');
        FileUploader.value = '';
    }

    onFormSubmit(e: any) {
        e.preventDefault();
        if (this.state.valid) {
            let formData: FormData = new FormData();
            let isNew: number = (this.state.alreadyExisting)?0:1;

            formData.append('file', this.state.toUpload);
            formData.append('project_id', this.state.projectID);
            formData.append('isnew', isNew.toString());

            fetch('/add-file/', {
                method: 'POST',
                body: formData
            })
            .then((response: any) => {
                response.json().then((data: AddFileResult) => {
                    let result = data.result;
                    if (!result.err && !this.state.alreadyExisting) {
                        // emit an event
                        this.props.eventManager.getEmitter().emit('file-added', {
                            fileName: this.state.filename,
                            uploadPath: result.upload_path,
                            modified: result.modified
                        });
                    }

                    // clear state
                    this.setState({
                        message: result.message,
                        filename: 'No File Selected',
                        size: 0,
                        valid: false
                    });

                    // reset the uploader
                    let FileUploader: HTMLInputElement = document.querySelector('input[name="specs_file"]');
                    FileUploader.value = '';

                    setTimeout(() => {
                        this.setState({
                            message: ''
                        });
                    }, 1500)
                })
            })
            .catch((err: any) => {
                console.log(err);
            });
        }
    }

    render() {
        let toDisplay: React.CSSProperties = {
            display: (this.props.toDisplay)?'block':'none'
        }
        return (
            <div>
                <form action="post" style={toDisplay} onSubmit={(e: any) => this.onFormSubmit(e)}>
                    <div className="field">
                        <div className="file has-name is-link is-small">
                            <label htmlFor="specs_file" className="file-label">
                                <input className='file-input' onChange={((e: any) => this.onFileSelect(e))} type="file" id="specs_file" name="specs_file" accept='.json,.yaml' required/>
                                <span className='file-cta'>
                                    <span className='file-icon'>
                                        <i className='fas fa-file-import'></i>
                                    </span>
                                    <span className='file-label'>
                                        Choose A Specs File...
                                    </span>
                                </span>
                                <span className="file-name">
                                    {this.state.filename}
                                </span>
                            </label>
                        </div>
                    </div>
                    <div className="field">
                        <div className="level">
                            <div className="level-left">
                                <div className="control level-item">
                                    <input className='button is-primary' type="submit" value="Upload" disabled={(this.state.valid?false:true)}/>
                                </div>
                                <div className="control level-item">
                                    <button className='button' onClick={((e: any) => this.onCancelUpload(e))} disabled={(this.state.valid?false:true)}>
                                        Cancel
                                    </button>
                                </div>
                                <label htmlFor="system_message" className='level-item'>
                                    {this.state.message}
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}