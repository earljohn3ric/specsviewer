import { EventEmitter } from 'fbemitter';

export class EventManager {
    emitter: EventEmitter
    constructor() {
        this.emitter = new EventEmitter();
    }

    getEmitter(): EventEmitter {
        return this.emitter;
    }
}
