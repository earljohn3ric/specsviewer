import * as React from 'react';
import fetch from 'node-fetch';

import { ListHeaderState, AddProjectResult, ListHeaderProps } from '../interfaces/interfaces';
import { EventManager } from './EventManager';
import { setTimeout } from 'timers';

export class ListHeader extends React.Component<ListHeaderProps, ListHeaderState> {
    constructor (props: any) {
        super(props);

        this.state = {
            isAdding: false,
            projectName: '',
            hasError: false,
            message: ''
        }
    }

    toggleAdd() {
        this.setState({
            isAdding: !(this.state.isAdding)
        });
    }

    formSubmit(e: any) {
        e.preventDefault();
        fetch("/add-project/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                project_name: this.state.projectName
            })
        })
        .then((response: any) => {
            response.json().then((data: AddProjectResult) => {
                let result = data.result;
                if (!result.err) {
                    this.setState({
                        hasError: result.err,
                        message: result.message
                    })
                    if (!this.state.hasError) {
                        this.props.eventManger.getEmitter().emit('new-project', {
                            projectID: result.projectID,
                            projectName: this.state.projectName
                        })
                    }
                } else {
                    this.setState({
                        message: result.message
                    });
                }
                setTimeout(() => {
                    this.setState({
                        message: ''
                    });
                }, 1500)
            })
        })
        .catch((error: any) => {
            console.log(error)
        })
    }

    inputChange(e: any) {
        this.setState({
            projectName: e.target.value,
            hasError: false,
            message: ''
        })
    }

    render() {
        let isAdding
        if (this.state.isAdding) {
            isAdding = (
                <form method='post' onSubmit={((e)=>this.formSubmit(e))}>
                    <div className="field">
                        <div className="control">
                            <input className='input is-rounded' type='name' name='project_name' onChange={((e)=>this.inputChange(e))} placeholder='Project Name' required />
                        </div>
                    </div>
                    <div className="field is-grouped">
                        <div className="control">
                            <button className='button is-success'>Add</button>
                        </div>
                        <div className="control">
                            <button className='button' onClick={(()=>this.toggleAdd())}>Cancel</button>
                        </div>
                    </div>
                </form>
            )
        } else {
            isAdding = (
                <div className='field'>
                    <div className="control">
                        <button className='button is-link' onClick={(()=>this.toggleAdd())}>New Project</button>
                    </div>
                </div>
            )
        }
        return (
            <div>
                <section>
                    <h1 className='title'>Open API Viewer</h1>
                </section>
                <section className="section">
                    {isAdding}
                    <span className={(!this.state.hasError?'success':'error')}>
                        {this.state.message}
                    </span>
                </section>
            </div>
        )
    }
}