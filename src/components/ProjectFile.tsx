import * as React from 'react';
import fetch from 'node-fetch';

import { FileProps, DeleteFileResult } from '../interfaces/interfaces';

export class ProjectFile extends React.Component<FileProps> {
    constructor(props: any) {
        super(props)
    }

    onDeleteFile(e: any) {
        e.preventDefault();
        fetch('/delete-file/', {
            method: 'DELETE',
            body: JSON.stringify({
                project_id: this.props.projectID,
                file_name: this.props.file.name,
                path: this.props.file.path
            })
        }).then((response: any) => {
            response.json().then((data: DeleteFileResult) => {
                let result = data.result;
                if (!result.err) {
                    this.props.onDeleteFile(this.props.file.name);
                }
            })
        });
    }

    render() {
        return (
            <div className="column is-one-quarter">
                <div className="card">
                    <header className="card-header">
                        <p className="card-header-title">
                            <label>{this.props.file.name}</label>
                        </p>
                        <a href="#" className="card-header-icon" onClick={((e: any) => this.onDeleteFile(e))}>
                            <span className="icon">
                                <i className="fas fa-times-circle"></i>
                            </span>
                        </a>
                    </header>
                    <div className="card-content">
                        <div className="content has-text-centered is-small">
                            <span className="subtitle is-2">
                                <i className="fas fa-file-alt"></i>
                            </span>
                            <br/>
                            <label htmlFor="">Date Modified: {this.props.formattedDate}</label>
                        </div>
                    </div>
                    <footer className="card-footer">
                        <a href={`/redoc/?specs=${this.props.file.path}`} className='card-footer-item' target='_blank'>
                            <span>
                                <i className="fas fa-eye"></i> View Content
                            </span>
                        </a>
                    </footer>
                </div>
            </div>
        )
    }
}