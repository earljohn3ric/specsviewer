import * as React from 'react';
import fetch from 'node-fetch';
import * as moment from 'moment';

import { ProjectDetails, ProjectInfoProps, GetFilesResult } from '../interfaces/interfaces';
import { FileUploader } from './FileUploader';
import { EventEmitter } from 'fbemitter';
import { ProjectFile } from './ProjectFile';

export class ProjectInfo extends React.Component<ProjectInfoProps, ProjectDetails> {
    constructor(props: any) {
        super(props);

        this.state = {
            _id: '',
            name: '',
            files: []
        }
    }

    componentDidMount() {
        let emitter: EventEmitter = this.props.eventManager.getEmitter();
        
        emitter.addListener('view-project', ((e: any) => this.onProjectView(e)));

        emitter.addListener('file-added', ((e: any) => this.onFileAdded(e)));
    }

    onFileAdded(e: any) {
        let files = this.state.files;
        files.push({
            name: e.fileName,
            modified: e.modified,
            path: e.uploadPath
        });

        this.setState({
            files: files
        });
    }

    async onProjectView(e: any) {
        await this.setState({
            _id: e.id,
            name: e.name
        });

        fetch(`/get-files/${this.state._id}`)
        .then((response: any) => {
            response.json().then((data: GetFilesResult) => {
                let result = data.result
                this.setState({
                    files: result.files
                });
            });
        });
    }

    onDeleteFile(fileName: string) {
        let files = this.state.files;
        let newFiles: Array<any> = [];
        files.forEach(file => {
            if (file.name != fileName) {
                newFiles.push(file)
            }
        })

        this.setState({
            files: newFiles
        });
    }

    render() {
        let toDisplayUploader = (this.state._id != '')?true:false;
        let files = this.state.files.map((file, index) => {
            let formattedDate = moment(file.modified).format('MM DD, YYYY h:mm A')
            return (
                <ProjectFile 
                    key={index}
                    projectID={this.state._id}
                    file={file}
                    formattedDate={formattedDate}
                    onDeleteFile={((e: any) => this.onDeleteFile(e))}
                />
            )
        })

        return (
            <div>
                <div className="projectInfoHeader">
                    <h3 className='subtitle is-3'>{this.state.name}</h3>
                    <FileUploader eventManager={this.props.eventManager} toDisplay={toDisplayUploader}/>
                </div>
                <section className="section">
                    <h5 className="subtitle is-5">
                        {((this.state.files.length > 0)?'Open API Specs File':'')}
                    </h5>
                </section>
                <section className="section">
                    <div className="columns is-multiline">
                        {files}
                    </div>
                </section>
            </div>
        );
    }
}