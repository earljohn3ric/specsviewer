import { EventManager } from "../components/EventManager";

export interface AddProjectResult {
    result: {
        err: boolean
        message: string
        projectID: string
    }
}

export interface GetProjectResult {
    result: {
        err: boolean
        message: string
        projects: Array<ProjectDetails>
    }
}

export interface GetFilesResult {
    result: {
        err: boolean
        message: string
        files: Array<ProjectDetailsFiles>
    }
}

export interface DeleteFileResult {
    result: {
        err: boolean
        message: string
    }
}

export interface VerifyFileResult {
    result: {
        err: boolean
        message: string
        valid: boolean
        existing: boolean
    }
}

export interface AddFileResult {
    result: {
        err: boolean
        message: string
        upload_path: string
        modified: string
    }
}

export interface ProjectDetails {
    _id: string
    name: string
    files?: Array<ProjectDetailsFiles>
}

interface ProjectDetailsFiles {
    name: string
    modified: string
    path: string
}

export interface ListHeaderState {
    isAdding: boolean
    projectName: string
    hasError: boolean
    message: string
}

export interface ListHeaderProps {
    eventManger: EventManager
}

export interface ListBodyState {
    hasContent: boolean
    contents: Array<ProjectDetails>
}

export interface ListBodyProps {
    eventManger: EventManager
}

export interface ProjectInfoState {}

export interface ProjectInfoProps {
    eventManager: EventManager
}

export interface FileUploaderProps {
    eventManager: EventManager
    toDisplay: boolean
}

export interface FileUploadState {
    filename: string
    size: number
    valid: boolean
    message: string
    projectID: string
    toUpload: File
    alreadyExisting: boolean
}

export interface FileProps {
    projectID: string,
    file: ProjectDetailsFiles
    formattedDate: string
    onDeleteFile(fileName: string): void
}