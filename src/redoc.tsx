import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { RedocStandalone } from 'redoc';

import getUrlParameter from './components/Functions';
console.log(getUrlParameter('specs'));

ReactDOM.render(
    <RedocStandalone 
        specUrl={getUrlParameter('specs')} 
        options={{
            nativeScrollbars: true
        }}
        onLoaded={((e) => {
            console.log(e)
        })}    
    />,
    document.querySelector('div#Redoc')
);